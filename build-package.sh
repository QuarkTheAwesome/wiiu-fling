#!/bin/bash

source build.env
dl_url="URL_`echo $CI_JOB_NAME | sed 's/-/_/g'`"
should_build="BUILD_`echo $CI_JOB_NAME | sed 's/-/_/g'`"
if [ "${!should_build}" != "no" ]
then
    pacman -Sy
    source /etc/profile.d/*
    cd $CI_PROJECT_DIR/packages/$CI_JOB_NAME
    chmod -R 0777 .
    useradd builduser -m
    passwd -d builduser
    echo "builduser ALL=(ALL) ALL" | tee -a /etc/sudoers
    sudo -E -u builduser makepkg -s --noconfirm
else
    cd $CI_PROJECT_DIR/packages/$CI_JOB_NAME
    wget ${!dl_url}
fi
