FROM archlinux/base:latest
COPY dkp /dkp
COPY fling-key.pub /fling-key.pub

RUN cd /dkp && chmod +x install-dkp.sh && ./install-dkp.sh && \
    pacman-key --add /fling-key.pub && pacman-key --lsign 6F986ED22C5B9003 && \
    pacman -S --needed --noconfirm base-devel wget unzip && \
    pacman -S --needed --noconfirm devkitPPC wut && \
    pacman -Scc --noconfirm

# change this comment to force a docker rebuild - 1
