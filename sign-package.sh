#~/bin/bash

source build.env
dl_url="URL_`echo $CI_JOB_NAME | sed 's/-/_/g'`"
should_build="BUILD_`echo $CI_JOB_NAME | sed 's/-/_/g'`"
if [ "${!should_build}" != "no" ]
then
    if [ -z "$FLING_SIGN_KEY" ]
    then
        echo "No signing key, in staging mode..."
    else
        cd $CI_PROJECT_DIR
        gpg --batch --import fling-key.pub
        echo "$FLING_SIGN_KEY" | gpg --batch --import
        cd $CI_PROJECT_DIR/packages/$CI_JOB_NAME
        echo Signing $CI_JOB_NAME...
        gpg --batch --detach-sign `find -name '*.pkg*' -a ! -name '*.sig'`
    fi
else
    cd $CI_PROJECT_DIR/packages/$CI_JOB_NAME
    wget ${!dl_url}.sig
fi
