#!/bin/bash

FLING_URL=https://fling.heyquark.com
FLING_REPO=wiiu-fling.db

mkdir db
wget $FLING_URL/$FLING_REPO
tar xvf $FLING_REPO -C db
for i in db/*; do
    VERSION=`grep -A1 VERSION $i/desc | grep -v VERSION`
    PACKAGE=`grep -A1 BASE $i/desc | grep -v BASE`
    FILENAME=`grep -A1 FILENAME $i/desc | grep -v FILENAME`
    PACKAGEDIR=`echo $PACKAGE | sed 's/wiiu-//g'`
    source packages/$PACKAGEDIR/PKGBUILD

    echo "======================"
    echo "Package: $PACKAGE (packages/$PACKAGEDIR)"
    echo "Local: $pkgver-$pkgrel"
    echo "Fling: $VERSION"

    VERDIFF=`vercmp $pkgver-$pkgrel $VERSION`
    PACKID=`echo $PACKAGEDIR | sed 's/-/_/g'`
    if [ "$VERDIFF" == "0" ]; then
        echo "Will download from $FLING_URL/$FILENAME"
        echo "BUILD_$PACKID=no" >> build.env
        echo "URL_$PACKID=$FLING_URL/$FILENAME" >> build.env
    elif [ "$VERDIFF" == "1" ]; then
        echo "Will build new package"
        echo "BUILD_$PACKID=yes" >> build.env
    elif [ "$VERDIFF" == "-1" ]; then
        echo "Remote newer? Will download from $FLING_URL/$FILENAME"
        echo "BUILD_$PACKID=no" >> build.env
        echo "URL_$PACKID=$FLING_URL/$FILENAME" >> build.env
    fi
    echo
done
