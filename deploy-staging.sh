#!/bin/bash
cd $CI_PROJECT_DIR/public/ && tar -cvJf ../packages.tar.xz .
cd $CI_PROJECT_DIR && jo packagezip=%packages.tar.xz | curl -f -v \
    -H "Content-Type:application/json" \
    -H "X-Secret:$STAG_DEPLOY_SECRET" \
    -X POST -d @- https://umbreon.heyquark.com/cgi/hooks/fling-deploy-staging
