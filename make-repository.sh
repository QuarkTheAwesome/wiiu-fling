#!/bin/bash

FLING_URL=https://fling.heyquark.com
FLING_REPO=wiiu-fling.db.tar.gz
FLING_FILES=wiiu-fling.files.tar.gz

mkdir -p $CI_PROJECT_DIR/public

cd $CI_PROJECT_DIR
source build.env
for pkgdir in packages/*; do
    PACKAGEDIR=`basename $pkgdir`
    PACKID=`echo $PACKAGEDIR | sed 's/-/_/g'`

    should_build="BUILD_$PACKID"
    if [ "${!should_build}" != "no" ]
    then
        cp $pkgdir/*.pkg* $CI_PROJECT_DIR/public
    fi
done

cd $CI_PROJECT_DIR/public
wget $FLING_URL/$FLING_REPO
wget $FLING_URL/$FLING_FILES

ls -Ral

if [ -z "$FLING_SIGN_KEY" ]
then
    echo "No signing key, in staging mode..."
    repo-add $FLING_REPO `find -name '*.pkg*' -a ! -name '*.sig'`
else
    gpg --batch --import $CI_PROJECT_DIR/fling-key.pub
    echo "$FLING_SIGN_KEY" | gpg --batch --import
    repo-add --sign $FLING_REPO `find -name '*.pkg*' -a ! -name '*.sig'`
fi
